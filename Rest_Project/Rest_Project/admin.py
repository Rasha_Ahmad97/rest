from django.contrib import admin
from .models import Car, Seller, Images

admin.site.register(Car)
admin.site.register(Seller)
admin.site.register(Images)

