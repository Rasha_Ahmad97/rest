from django.contrib import admin
from django.urls import path
from Rest_Project import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("admin/", admin.site.urls),
    path("get_cars/", views.get_cars),
    path("search_Car/<str:pk>/", views.get_car_byname),
    path("get_sellers/", views.get_sellers),
    path("add_seller/", views.add_seller),
    path("add_car/", views.add_car),
    path("add_images/", views.add_images),
    path("media/images/<str:pk>/", views.get_image),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

