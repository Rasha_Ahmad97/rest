from django.http import JsonResponse
from rest_framework.decorators import api_view
from .serializers import CarSerializer, ImageSerializers, SellerSerializer
from .models import Car, Images, Seller
from rest_framework import status
from .helpers import modify_input_for_multiple_files


from rest_framework.response import Response

# Get all the cars
@api_view(["GET"])
def get_cars(request):
    cars = Car.objects.all()
    serializer = CarSerializer(cars, many=True)
    return JsonResponse({"cars": serializer.data})


# Get car by name
@api_view(["GET"])
def get_car_byname(request, pk):
    car = Car.objects.filter(title=pk)
    serializer = CarSerializer(car, many=True)
    return Response(serializer.data)


# Get all sellers
@api_view(["GET"])
def get_sellers(request):
    seller = Seller.objects.all()
    serializer = SellerSerializer(seller, many=True)
    return Response(serializer.data)


# Get all sellers
@api_view(["POST"])
def add_seller(request):
    serializer = SellerSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def add_car(request):
    serializer = CarSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Add images for car
@api_view(["POST"])
def add_images(request):
    offer_id = request.data["offer_id"]
    images = dict((request.data).lists())["photo"]
    flag = 1
    arr = []
    for image in images:
        modified_data = modify_input_for_multiple_files(offer_id, image)
        file_serilazers = ImageSerializers(data=modified_data)
        if file_serilazers.is_valid():
            file_serilazers.save()
            arr.append(file_serilazers.data)
        else:
            flag = 0
    if flag == 1:
        return Response(arr, status=status.HTTP_201_CREATED)
    else:
        return Response(arr, status=status.HTTP_400_BAD_REQUEST)


# Get image
@api_view(["GET"])
def get_image(request, pk):
    image = Images.objects.filter.URL(photo=pk)
    serializer = ImageSerializers(image, many=True)
    return Response(serializer.data)

