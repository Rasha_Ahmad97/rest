from django.db import models
from django.conf import settings

# define the model for the seller
class Seller(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)

    def __str__(self):
        return self.name


# define the car model
class Car(models.Model):
    seller_id = models.ForeignKey(Seller, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    price = models.CharField(max_length=200)
    album_id = models.CharField(max_length=200)
    type_of_vehicle = models.CharField(max_length=100)
    vehicle_number = models.CharField(max_length=50)
    mileage = models.CharField(max_length=100)
    power = models.CharField(max_length=100)
    fuel = models.CharField(max_length=100)
    fuel_consumption = models.CharField(max_length=100)
    number_of_doors = models.CharField(max_length=10)
    gearbox = models.CharField(max_length=100)
    year_model = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    color_interior = models.CharField(max_length=100)
    maximum_speed = models.CharField(max_length=100)
    number_of_seats = models.CharField(max_length=20)

    def __str__(self):
        return self.title


# define the model for images (each car has multiple images)
class Images(models.Model):
    offer_id = models.ForeignKey(Car, on_delete=models.CASCADE, related_name="cars")
    photo = models.ImageField(upload_to="images", blank=True, null=True)

    def __str__(self):
        return "/media/" + self.photo.name
