from django.db.models import fields
from django.db.models.base import Model
from rest_framework import serializers
from .models import Car, Images, Seller


class ImageSerializers(serializers.ModelSerializer):
    class Meta:
        model = Images
        fields = ("offer_id", "photo")


class CarSerializer(serializers.ModelSerializer):
    cars = serializers.StringRelatedField(many=True)

    class Meta:
        model = Car
        fields = (
            "seller_id",
            "title",
            "description",
            "make",
            "model",
            "price",
            "album_id",
            "type_of_vehicle",
            "vehicle_number",
            "mileage",
            "power",
            "fuel",
            "fuel_consumption",
            "number_of_doors",
            "gearbox",
            "year_model",
            "color",
            "color_interior",
            "maximum_speed",
            "number_of_seats",
            "cars",
        )


class SellerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seller
        fields = ("name", "email", "phone")
